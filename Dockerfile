FROM centos:7

COPY MariaDB.repo /etc/yum.repos.d/MariaDB.repo
COPY run-mysql.sh /run-mysql.sh

## Install Chrome
RUN yum install https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm -y

## Install MyQL 5.7
RUN yum install http://dev.mysql.com/get/mysql57-community-release-el7-11.noarch.rpm -y \
  && yum install --enablerepo=mysql57-community mysql-community-server -y

## Install PHP 7.2
RUN yum install epel-release -y \
  && yum-config-manager --disable epel \
  && yum install http://rpms.famillecollet.com/enterprise/remi-release-7.rpm -y \
  && yum-config-manager --disable remi-safe \
  && yum install git -y \
  && yum install --enablerepo=epel,remi,remi-php72 php php-mysql php-pdo php-xml php-devel php-pear php-mbstring php-zip -y \
  && yum clean all

## Install Git
RUN yum update -y \
  && yum install git -y

# Setup Xdebug
RUN pecl install xdebug-2.4.1

ENV PHP_INI_DIR="/etc"
ENV PHP_TIMEZONE="Asia/Tokyo"

# Memory Limit
RUN echo "memory_limit=-1" > $PHP_INI_DIR/php.d/memory-limit.ini

# Time Zone
RUN echo "date.timezone=${PHP_TIMEZONE:-UTC}" > $PHP_INI_DIR/php.d/date_timezone.ini

# Xdebug
RUN echo "zend_extension=/usr/lib64/php/modules/xdebug.so" > $PHP_INI_DIR/php.d/xdebug.ini

# Register the COMPOSER_HOME environment variable
ENV COMPOSER_HOME /composer

# Add global binary directory to PATH and make sure to re-export it
ENV PATH /composer/vendor/bin:$PATH

# Allow Composer to be run as root
ENV COMPOSER_ALLOW_SUPERUSER 1

# Setup the Composer installer
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
  && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
  && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" \
  && php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer
