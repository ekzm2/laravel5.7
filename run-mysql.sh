#!/bin/bash
set -e

mysql_install_db --user=mysql --ldata=/var/lib/mysql/ && \
mysqld_safe &
